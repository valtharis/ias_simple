<?php
/*
 * provider 2 database settings
 */
$host = "localhost";
$database = "ias_provider2";
$dbusername = "provider2";
$dbpassword = "provider";


$pdo = new PDO('mysql:host='.$host.';dbname='.$database.';charset=utf8mb4', $dbusername, $dbpassword);
$stmt = $pdo->query("SELECT * FROM magazines");
$results =  $stmt->fetchAll(PDO::FETCH_ASSOC);


header('Content-Type: application/json');
echo json_encode($results);