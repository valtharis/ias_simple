<?php
// take data from providers
$provider1Response = file_get_contents('http://localhost/ias_simple/provider1');
$provider2Response = file_get_contents('http://localhost/ias_simple/provider2');

// parse data from JSON to arrays
$books =  json_decode($provider1Response, true);
$magazines = json_decode($provider2Response, true);

// transform arrays with data and merge it
$items = array_merge(convertBooks($books), convertMagazines($magazines));

// make some random order in response
shuffle($items);

// show response in JSON
header('Content-Type: application/json');
echo json_encode($items);



function convertBooks($books){
    $result = [];
    foreach ($books as $book){
        $item = [
            'provider_item_id' => $book['id'],
            'tytul' => $book['title'],
            'autorzy' => $book['authors'],
            'opis' => $book['description'],
            'strony' => $book['pages'],
            'wydawca' => $book['publisher'],
            'typ' => 'book'
        ];
        array_push($result, $item);
    }
    return $result;
}
function convertMagazines($magazines){
    $result = [];
    foreach ($magazines as $magazine){
        $item = [
            'provider_item_id' => $magazine['id'],
            'tytul' => $magazine['name'],
            'autorzy' => $magazine['author'],
            'opis' => $magazine['short_description'],
            'strony' => $magazine['pages'],
            'wydawca' => $magazine['published_by'],
            'typ' => 'magazine'
        ];
        array_push($result, $item);
    }
    return $result;
}